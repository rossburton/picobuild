# picobuild

A pico-scale PEP-517 Python package builder, designed primarily for source-based
distributions to use.

This means the command-line interface is lean, there is no venv support, and it
vendors any dependencies so it can be used to bootstrap a Python environment
from scratch.

## Example Usage

```
$ python3 -m picobuild --source . --dest dist --wheel
Wrote wheel /home/ross/Code/picobuild/dist/picobuild-0.1-py2.py3-none-any.whl
```

## Building

Picobuild uses `flit_core` to build, so once that is installed picobuild can build itself:

```
$ pip install --user flit_core
$ python3 -m picobuild --source . --dest dist --sdist
Wrote wheel /home/ross/Code/picobuild/dist/picobuild-0.1-py2.py3-none-any.whl
```

## Bootstrapping

Picobuild can be used in a distribution packaging system with minimal
bootstrapping, for example a setup using `picobuild` and `installer` could
bootstrap as follows:

1. Self-build `flit_core` with `python3 -mflit_core.wheel`
2. Install `flit_core` with `unzip` or `flit_core/bootstrap_install.py`
3. Build `installer` with `python3 -mflit_core.wheel`
4. Self-install `installer` with `python3 -minstaller`
5. Self-build `picobuild` with `python3 -mpicobuild`
6. Install `picobuild` with `installer`
7. Now all other packages can use `picobuild` and `installer` to build and install.

## Updating Vendored Libraries

The vendorered dependencies can be updated using `vendoring`:

```
$ python3 -mvendoring sync
```

## License

Picobuild is MIT licensed, see `LICENSE` for terms.

Picobuild also embeds a number of extra packages under `picobuild/vendored/` which
are used if they're not available.  These are licensed as follows:

- packaging: either:
  - Apache-2.0 (`picobuild/vendored/packaging/LICENSE.APACHE`)
  - BSD-2-Clause (`picobuild/vendored/packaging/LICENSE.BSD`)
- pyproject-hooks: MIT (`picobuild/vendored/pep517/LICENSE`)
- pyparsing: MIT (`picobuild/vendored/pyparsing/LICENSE`)
- tomli: MIT (`picobuild/vendored/tomli/LICENSE`)