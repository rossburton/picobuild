from setuptools import setup

setup(
    name="legacy",
    version="1.0.0",
    url="http://example.com/",
    author="Ross Burton",
    author_email="ross.burton@arm.com",
)
