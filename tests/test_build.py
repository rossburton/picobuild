import pathlib
import tempfile
import subprocess
import unittest
import zipfile

import picobuild

top = pathlib.Path(__file__).parent.parent


class BuildAPITests(unittest.TestCase):
    def do_build_sdist(self, testname, expected_filename):
        """
        Helper to build a sdist from a specific test directory and verify that
        the expected archive is built.
        """
        with tempfile.TemporaryDirectory(prefix=f"picobuild-{testname}") as destname:
            src = top / "tests" / testname
            dest = pathlib.Path(destname)

            builder = picobuild.Builder(src)
            missing = builder.check_sdist_dependencies()
            self.assertEqual(missing, set())
            filename = builder.build_sdist(dest)

            self.assertEqual(filename, expected_filename)
            sdist = dest / filename
            self.assertTrue(sdist.exists())

    def test_flit_core_sdist(self):
        self.do_build_sdist("flit", "test_flit-1.0.0.tar.gz")

    def test_flit_core_wheel(self):
        with tempfile.TemporaryDirectory(prefix="picobuild-test") as destname:
            src = top / "tests" / "flit"
            dest = pathlib.Path(destname)

            builder = picobuild.Builder(src)
            missing = builder.check_wheel_dependencies()
            self.assertEqual(missing, set())
            output = builder.build_wheel(dest)

            self.assertEqual(output, "test_flit-1.0.0-py2.py3-none-any.whl")
            wheel = dest / output
            self.assertTrue(wheel.exists())
            with zipfile.ZipFile(wheel) as zip:
                self.assertIn("test_flit/__init__.py", zip.namelist())

    def test_missing_bootstrap_depends(self):
        src = top / "tests" / "missing-depends"
        builder = picobuild.Builder(src)

        missing = builder.check_bootstrap_dependencies()
        self.assertEqual(missing, {("not_a_valid_dependency",)})

    def test_legacy(self):
        self.do_build_sdist("legacy", "legacy-1.0.0.tar.gz")


class BuildCLITests(unittest.TestCase):
    def do_build(self, testname, format, expected_filename):
        with tempfile.TemporaryDirectory(prefix=f"picobuild-{testname}") as destname:
            src = top / "tests" / testname
            dest = pathlib.Path(destname)

            command = (
                "python3",
                "-m",
                "picobuild",
                "--source",
                src,
                "--dest",
                dest,
                f"--{format}",
            )
            process = subprocess.run(
                command,
                cwd=top,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
                text=True,
            )

            output = dest / expected_filename
            self.assertIn(f"Wrote {format} {output}\n", process.stdout)
            self.assertTrue(output.exists())
            self.assertEqual(process.returncode, 0)

    def test_help(self):
        command = ("python3", "-m", "picobuild", "--help")
        process = subprocess.run(command, cwd=top, capture_output=True, text=True)
        self.assertIn("Pico-sized Python Package Builder", process.stdout)
        self.assertEqual(process.returncode, 0)

    def test_no_target(self):
        command = ("python3", "-m", "picobuild", "--source", top)
        process = subprocess.run(command, cwd=top, capture_output=True, text=True)
        self.assertIn("No target", process.stdout)
        self.assertEqual(process.returncode, 1)

    def test_flit(self):
        self.do_build("flit", "sdist", "test_flit-1.0.0.tar.gz")
        self.do_build("flit", "wheel", "test_flit-1.0.0-py2.py3-none-any.whl")

    def test_legacy(self):
        self.do_build("legacy", "sdist", "legacy-1.0.0.tar.gz")

    def test_missing_depends(self):
        with tempfile.TemporaryDirectory(prefix="picobuild-test") as destname:
            src = top / "tests" / "missing-depends"
            dest = pathlib.Path(destname)

            command = (
                "python3",
                "-m",
                "picobuild",
                "--source",
                src,
                "--dest",
                dest,
                "--wheel",
            )
            process = subprocess.run(
                command,
                cwd=top,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
                text=True,
            )
            self.assertIn("not_a_valid_dependency", process.stdout)
            self.assertEqual(process.returncode, 1)
